import scrapy
import base64
import requests
import re
from .log import log_text as log            # 文本数据，where

class Movie(scrapy.Spider):
    name = 'all'
    def start_requests(self):                # 爬虫开始后默认运行
        log('开始执行爬虫','log')
        # 获取 beiwo888 排行榜
        for i in range(0,186):
            page = 186 - i
            if page <= 105:
                log('正在爬取 beiwo888 第 ' + str(page) + ' 页', 'log')
                if page == 1:
                    beiwo_url = 'http://www.beiwo888.com/list/8/'
                else:
                    beiwo_url = 'http://www.beiwo888.com/list/8/index-' + str(page) + '.html'
                yield scrapy.Request(url=beiwo_url, callback=self.beiwo888_rank)
            log('正在爬取 ygdy8 第 ' + str(page) + ' 页', 'log')
            ygdy8_url = 'http://www.ygdy8.net/html/gndy/dyzz/list_23_' + str(page) + '.html'
            yield scrapy.Request(url=ygdy8_url, callback=self.ygdy8_rank)

        # 获取 ygdy8 排行榜
        # yield scrapy.Request(url='http://www.ygdy8.net/', callback=self.ygdy8_rank)

    def beiwo888_rank(self, response):      # 获取 beiwo888 排行榜 并执行爬虫获取
        select = response.xpath('//li/a[@title="[videolist:name]"]/@href').extract()
        for i in select:
            url = 'http://www.beiwo888.com' + i
            yield scrapy.Request(url=url, callback=self.beiwo888)

    def beiwo888(self, response):          # 从 beiwo888 url 获取电影数据
        name = response.xpath('//h1/text()').extract()[0]
        type = response.xpath('//div[@class="location"]/a[not(@href="/")]/text()').extract()[0]
        stars = response.xpath('//li/a[@target="_blank"]/text()').extract()
        star = ''
        for i in stars:
            star = star + i + '、'
        country = response.xpath('//li/text()').extract()[-2]
        country = country.split('地区：')[1]
        time = response.xpath('//li/text()').extract()[0]
        time = time.split('上映年代：')[1][:4]
        download = response.xpath('//script[not(@type or @src)]').extract()[1]
        download = download.split('$')[1]
        download = to_thunder(download)
        movie = {
            'name':name,
            'type':type,
            'star':star,
            'country':country,
            'time':time,
            'download':download
        }
        add_movie(movie, 'beiwo888')

    def ygdy8_rank(self, response):         # 获取 ygdy8 排行榜 并执行爬虫获取
        select = response.xpath('//b/a/@href').extract()
        for i in select:
            url = 'http://www.ygdy8.net' + i
            yield scrapy.Request(url=url, callback=self.ygdy8)

    def ygdy8(self, response):              # 从 ygdy8 url 获取电影数据
        name = response.xpath('//title/text()').extract()[0]
        name = re.findall(r"《(.*?)》", name)[0]
        text = response.xpath('//td[not(@colspan)]').extract()[0]
        type = text.split('类　　别')[1]
        num = type.find('br')
        type = type.split(" ")[0][1:num - 1]
        star = text.split('主　　演')[1]
        star = star.split(" ")[0][1:]
        time = text.split('年　　代')[1]
        num = time.find('br')
        time = time.split(" ")[0][1:5]
        if text.find('产　　地') != -1:
            country = text.split('产　　地')[1]
            country = country.split(" ")[0][1:]
            downloads = response.xpath('//a[@href]/@href').extract()
            for i in downloads:
                if i.find("magnet") != -1:
                    download = i
                    break
        else:
            country = text.split('国　　家')[1]
            num = country.find('br')
            country = country.split(" ")[0][1:num-1]
            downloads = response.xpath('//a[@href]/@href').extract()
            for i in downloads:
                if i.find("ftp://") != -1:
                    download = to_thunder(i)
                    break
        movie = {
            'name': name,
            'type': type,
            'star': star,
            'country': country,
            'time': time,
            'download': download
        }
        add_movie(movie, 'ygdy8')

    def iidvd_rank(self, response):         # 获取 iidvd 排行榜 并执行爬虫获取
        pass

    def iidvd(self, response):              # 从 iidvd url 获取电影数据
        pass


def to_thunder(url):                            # 将下载链接转化未迅雷链接
    url = 'AA' + url + 'ZZ'
    url = base64.b64encode(url.encode('utf-8'))     # 将 url 64位加密
    url = 'thunder://' + str(url,encoding='utf-8')  # 将 字节集 转换为文本型并添加前缀
    return url

def add_movie(movie, where):                    # 将电影上传至服务器
    r = requests.get('http://120.76.63.106/addmv', params=movie)
    if r.text.find("电影添加成功") != -1:
        log(movie['name'] + '  添加成功', where)
    elif r.text.find("电影已存在") != -1:
        log(movie['name'] + '  已存在', where)
    else:
        log(movie['name'] + '  失败', where)