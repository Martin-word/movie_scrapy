import datetime, os


def get_day():                                              # 获取当前日期
    return datetime.datetime.now().strftime('%Y-%m-%d')
def get_time():                                             # 获取当前时间
    return datetime.datetime.now().strftime('%H:%M:%S')

def makedirs(where):                                        # 创建目录
    path = 'log/' + where
    if os.path.exists(path) == False:
        os.makedirs(path)
    return path

def log_text(text, where):                                  # 写入日志
    where = makedirs(where)
    path = where + '/' + get_day() + '.txt'
    print(path)
    fileName = path
    f = open(fileName, "a+")  # 追加写入文件
    f.write('\n')  # 换行
    f.write(get_time())
    f.write('  ' + str(text))  # 写入名言内容
    f.close()  # 关闭文件操作